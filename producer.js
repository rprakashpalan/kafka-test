// Kafka
var kafka = require('kafka-node');
var HighLevelProducer = kafka.HighLevelProducer;
var KeyedMessage = kafka.KeyedMessage;
var Client = kafka.Client;


var client = new Client('localhost:2181', 'my-client-id', {
    sessionTimeout: 300,
    spinDelay: 100,
    retries: 2
});

// For this demo we just log client errors to the console.
client.on('error', function(error) {
    console.error(error);
});

var producer = new HighLevelProducer(client);

console.info('Created producer.');

producer.on('ready', function() {
    // Create message and encode to Avro buffer
    //    var messageBuffer = type.toBuffer({
    //        enumField: 'sym1',
    //        id: '3e0c63c4-956a-4378-8a6d-2de636d191de',
    //        timestamp: Date.now()
    //    });

    // Create a new payload
    var payload = [{
        topic: 'v3-test',
        messages: ['msg1', 'msg2'],
        attributes: 0 /* No compression, 1: GZip, 2: snappy */
    }];

    //Send payload to Kafka and log result/error
    producer.send(payload, function(error, result) {
        console.info('Sent payload to Kafka: ', payload);
        if (error) {
            console.error(error);
        } else {
            var formattedResult = result[0]
            console.log('result: ', result)
        }
    });
});

// For this demo we just log producer errors to the console.
producer.on('error', function(error) {
    console.error(error);
});