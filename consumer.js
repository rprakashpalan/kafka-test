var kafka = require('kafka-node');
var HighLevelConsumer = kafka.HighLevelConsumer;
var Client = kafka.Client;

var client = new Client('localhost:2181');
var topics = [{
    topic: 'v3-test'
}];

var options = {
    autoCommit: true,
    fetchMaxWaitMs: 1000,
    fetchMaxBytes: 1024 * 1024,
    encoding: 'buffer'
};
var consumer = new HighLevelConsumer(client, topics, options);

console.info('Created consumer');

consumer.on('message', function(message) {
    console.info('Received message.')
    console.info(message)
    if (!Buffer.isBuffer(message.value)) {
        console.error('Message does not contain a buffer!')
    }
    console.info(message.value.toString())
        // console.info(Buffer.isBuffer(message.value))

    //var buf = new Buffer(message.value, 'binary'); // Read string into a buffer.
    // var decodedMessage = type.fromBuffer(buf.slice(0)); // Skip prefix.
    //console.log(decodedMessage);
});

consumer.on('error', function(err) {
    console.log('error', err);
});

process.on('SIGINT', function() {
    consumer.close(true, function() {
        process.exit();
    });
});